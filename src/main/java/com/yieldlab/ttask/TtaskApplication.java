package com.yieldlab.ttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class TtaskApplication {
	/**
	 * Comments to the solution:
	 * In production code I would do differently the following things:
	 * 1) Use abstractions. For the sake of Test task that would be an overkill.
	 * 2) Logging and error handling - since requirements for this are unknown, I did not add logging/error handling.
	 * 3) Tracing and metrics - since this is a HA service with strict SLO, would be very handy to know the exact time each rest call takes
	 */


	public static void main(String[] args) {
		SpringApplication.run(TtaskApplication.class, args);
	}

}
