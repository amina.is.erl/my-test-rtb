package com.yieldlab.ttask.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class BidRequest {

    Integer id;
    Map<String, Object> attributes;


}
