package com.yieldlab.ttask.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BidResponse {

    Integer id;
    BigDecimal bid;
    String content;

    public String getContentString () {
        return content.replace("$price$", bid.toPlainString());
    }

}
