package com.yieldlab.ttask.rest;

import com.yieldlab.ttask.model.BidRequest;
import com.yieldlab.ttask.model.BidResponse;
import com.yieldlab.ttask.service.BiddingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@AllArgsConstructor
public class BiddingController {

    private final BiddingService biddingService;

    @GetMapping("/{id}")
    public String getAuctionResult(@PathVariable Integer id, @RequestParam Map<String, Object> attributes) {
        return biddingService.getAuctionResult(new BidRequest(id, attributes));
    }

}
