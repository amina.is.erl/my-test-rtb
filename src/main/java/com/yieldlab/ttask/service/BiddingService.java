package com.yieldlab.ttask.service;

import com.yieldlab.ttask.model.BidRequest;
import com.yieldlab.ttask.model.BidResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BiddingService {

    private WebClient webClient;

    @Value("${bidders.hosts}")
    private final String[] BIDDERS_STRING;

    public String getAuctionResult(BidRequest bidRequest) {
        List<String> bidders = List.of(BIDDERS_STRING);
        List<Mono<BidResponse>> responses = bidders.stream().map(bidder -> getBidResponse(bidder, bidRequest)).collect(Collectors.toList());
        return responses.stream().map(Mono::block).max(Comparator.comparing(BidResponse::getBid)).get().getContentString();
    }

    private Mono<BidResponse> getBidResponse(String bidder, BidRequest bidRequest) {
        return webClient.post()
                .uri(bidder)
                .body(BodyInserters.fromValue(bidRequest))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(BidResponse.class);
    }

}
